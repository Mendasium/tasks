﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using TaskProject.Model;
using Task = System.Threading.Tasks.Task;

namespace TaskProject.Repositories
{
    public interface IRepository<TEntity> where TEntity: Entity
    {
        Task<IEnumerable<TEntity>> GetAsync(Func<TEntity, bool> filter = null);

        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int id);
        Task SaveAsync(CancellationToken token);
    }
}
