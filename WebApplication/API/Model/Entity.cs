﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Model
{
    public abstract class Entity
    {
        private static int id;
        public int Id { get; set; }

        public Entity()
        {
            Id = id;
            id++;
        }

        public abstract void Update(Entity entity);
    }
}
