﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskProject.Model.DTOModel
{
    public class DTOTeam : DTOEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
