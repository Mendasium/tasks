﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using TaskProject.Services.ServicesAbstraction;
using System.Threading.Tasks;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class TaskController : ControllerBase
    {
        ITaskService _tasksService;
        IMapper _mapper;
        public TaskController(ITaskService tasksService, IMapper mapper)
        {
            _mapper = mapper;
            _tasksService = tasksService;
        }

        [HttpPost]
        public async Task<DTOTask> Add([FromBody]DTOTask dTOTask)
        {
            return await _tasksService.Create(dTOTask);
        }

        [HttpPut]
        public async System.Threading.Tasks.Task Update([FromBody]DTOTask dTOTask)
        {
            await _tasksService.Update(dTOTask);
        }

        [HttpGet]
        public async Task<IEnumerable<DTOTask>> GetAll()
        {
            return await _tasksService.GetEntities();
        }

        [HttpGet("{id}")]
        public async Task<DTOTask> GetById(int id)
        {
            return (await _tasksService.GetEntities(x => x.Id == id)).FirstOrDefault();
        }

        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _tasksService.Delete(id);
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task Delete(DTOTask task)
        {
            await _tasksService.Delete(task);
        }

        [HttpGet("sortedBusyUsers")]
        public async Task<IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)>> GetSortedUsersWithTasks()
        {
            return await _tasksService.GetSortedUsersWithTasks();
        }

        [HttpGet("finishedTasks/{id}/{year}")]
        public async Task<IEnumerable<(int Id, string Name)>> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            return await _tasksService.GetFinishedTasksByUserId(id, year);
        }

        [HttpGet("shortNameTasks/{id}")]
        public async Task<IEnumerable<Model.Task>> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            return await _tasksService.GetShortnameTasksByUserId(id);
        }
    }
}
