﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class TeamController : ControllerBase
    {
        IService<DTOTeam> _teamsService;
        IMapper _mapper;
        public TeamController(IService<DTOTeam> teamsService, IMapper mapper)
        {
            _mapper = mapper;
            _teamsService = teamsService;
        }

        [HttpPost]
        public async Task<DTOTeam> Add([FromBody]DTOTeam dTOTeam)
        {
            return await _teamsService.Create(dTOTeam);
        }

        [HttpPut]
        public async Task Update([FromBody]DTOTeam dTOTeam)
        {
            await _teamsService.Update(dTOTeam);
        }

        [HttpGet]
        public async Task<IEnumerable<DTOTeam>> GetAll()
        {
            return await _teamsService.GetEntities();
        }

        [HttpGet("{id}")]
        public async Task<DTOTeam> GetById(int id)
        {
            return (await _teamsService.GetEntities(x => x.Id == id)).FirstOrDefault();
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _teamsService.Delete(id);
        }

        [HttpDelete]
        public async Task Delete(DTOTeam team)
        {   
            await _teamsService.Delete(team);
        }
    }
}
