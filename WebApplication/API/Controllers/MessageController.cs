﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;
using TaskProject.Services.ServicesAbstraction;
using System.Threading.Tasks;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        IMessageService _messageService;

        public MessagesController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        public async Task<IEnumerable<string>> GetAll()
        {
            return await _messageService.ReadAll();
        }
    }
}
