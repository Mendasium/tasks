﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using Task = TaskProject.Model.Task;

namespace TaskProject.Services.ServicesAbstraction
{
    public interface ITaskService : IService<DTOTask>
    {
        Task<IEnumerable<Task>> GetShortnameTasksByUserId(int id);
        Task<IEnumerable<(int Id, string Name)>> GetFinishedTasksByUserId(int id, int year = 2019);
        Task<IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)>> GetSortedUsersWithTasks();
    }
}
