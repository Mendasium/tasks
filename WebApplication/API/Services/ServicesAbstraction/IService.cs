﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Model.DTOModel;
using Task = System.Threading.Tasks.Task;

namespace TaskProject.Services
{
    public interface IService<TEntity> where TEntity : DTOEntity
    {
        Task<IEnumerable<TEntity>> GetEntities(Func<TEntity, bool> filter = null);

        Task<TEntity> Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
        Task Delete(int id);
    }
}
