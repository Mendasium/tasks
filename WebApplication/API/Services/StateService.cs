﻿using TaskProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Repositories;
using Microsoft.AspNetCore.SignalR;
using TaskProject.Services.ServicesAbstraction;
using System.Threading;
using Task = System.Threading.Tasks.Task;
using TaskProject.Model.DTOModel;
using AutoMapper;

namespace TaskProject.Services
{
    public class StateService : IService<DTOState>
    {
        IRepository<State> _states;
        IHubContext<AnswerHub> _hubContext;
        IQueueService _queueService;
        CancellationTokenSource token;
        IMapper _mapper;

        public StateService(IRepository<State> states, IHubContext<AnswerHub> hubContext, IQueueService queueService, IMapper mapper)
        {
            _queueService = queueService;
            _states = states;
            _hubContext = hubContext;
            _queueService = queueService;
            _mapper = mapper;
            token = new CancellationTokenSource();
        }

        public async Task<DTOState> Create(DTOState entity)
        {
            var newState = _mapper.Map<DTOState, State>(entity);

            _states.Create(newState);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {newState.ToString()} was created");

            var result = (await _states.GetAsync(x => x.Id == newState.Id)).First();
            return _mapper.Map<State, DTOState>(result);
        }

        public async Task Delete(DTOState entity)
        {
            var newState = _mapper.Map<DTOState, State>(entity);
            _states.Delete(newState);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {newState.ToString()} was removed");
        }

        public async Task Delete(int id)
        {
            _states.Delete(id);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {id} was removed");
        }

        public async Task<IEnumerable<DTOState>> GetEntities(Func<DTOState, bool> filter = null)
        {
            _queueService.Send($"Getting all States " + (filter == null ? "" : " with filter"));
            var result = await _states.GetAsync(_mapper.Map<Func<DTOState, bool>, Func<State, bool>>(filter));
            return _mapper.Map<IEnumerable<State>, IEnumerable<DTOState>>(result);
        }

        public async Task Update(DTOState entity)
        {
            var newState = _mapper.Map<DTOState, State>(entity);

            _states.Update(newState);
            await _states.SaveAsync(token.Token);
            _queueService.Send($"State {newState.ToString()} was updated");
        }
    }
}
