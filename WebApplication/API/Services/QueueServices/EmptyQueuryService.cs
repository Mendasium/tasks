﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Services.ServicesAbstraction;

namespace TaskProject.Services.QueueServices
{
    public class EmptyQueueService : IQueueService
    {
        public void Send(string message)
        { }
    }
}
