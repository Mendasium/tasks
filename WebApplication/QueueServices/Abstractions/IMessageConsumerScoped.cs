﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageConsumerScoped
    {
        IMessageConsumer MessageConsumer { get; }
        void Dispose();
    }
}
