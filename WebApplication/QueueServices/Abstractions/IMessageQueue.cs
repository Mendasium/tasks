﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageQueue
    {
        IModel Channel { get; set; }

        void DeclareExchange(string name, string type);
        void DeclareQueue(string exchangeName, string routingKey, string queueName);
        void Dispose();
    }
}
