﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageConsumer
    {
        void SetAcknowledge(ulong deliveryTag, bool processed);
        void Connect();
        event EventHandler<BasicDeliverEventArgs> Received;
    }
}
