﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Services
{
    public class ConnectionFactory : RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(Uri uri)
        {
            Uri = uri;
            //HostName = "localhost";
            RequestedConnectionTimeout = 5000;
            RequestedChannelMax = 10;
            RequestedHeartbeat = 30;
            NetworkRecoveryInterval = new TimeSpan(100);
            //TopologyRecoveryEnabled = true;
            //AutomaticRecoveryEnabled = true;
        }
    }
}
